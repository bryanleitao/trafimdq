<?php namespace Controllers;

use Models\persona;
use Models\vehiculo;
use Daos\PersonaMySqlDAO;
use Daos\VehiculoMySqlDAO;
use Config\Funciones;

class VehiculoController{

    public function __construct(){
        $this->DAO_Persona = PersonaMySqlDAO::getInstance();
        $this->DAO_Vehiculos = VehiculoMySqlDAO::getInstance();
    }
    /**
    *
    * muestra a partir del dni, si es que lo encuentra en la variable de sesion,
    * los vehiculos del titular, de lo contrario se redirige al AdminController/index
    *
    */
    public function vehiculos(){
        Funciones::comprobarSesion();

        if(!empty($_SESSION['dni'])){
            $this->persona = $persona = $this->DAO_Persona->traerPersonaPorDni($_SESSION['dni']);
            if(!isset($persona)){
                $_SESSION['dni'] = null;
                $alert = "No se encontro el DNI.";
                require "Views/admin.view.php";
            }else{
                $this->persona->set('vehiculos',$this->DAO_Vehiculos->traerVehiculos($_SESSION['dni']));
                //                Funciones::mostrarTodo($this->persona);
                require "Views/vehiculos.view.php";
            }
        }else{
            Funciones::redireccionar();
        }
    }
    /**
    *
    * valida todos los parametros y persiste los datos en la BD
    * @param $dominio string
    * @param $marca string
    * @param $modelo string
    * @param $anio $string
    *
    */
    public function agregar($dominio ="",$marca ="",$modelo ="",$anio =""){
        Funciones::comprobarSesion();
        $dni = $_SESSION['dni'];
        if(!empty($dni)){
            $this->persona = $persona = $this->DAO_Persona->traerPersonaPorDni($dni);
            if(!isset($persona)){
                $_SESSION['dni'] = null;
                $alert = "No se encontro el DNI.";
                require "Views/admin.view.php";
            }else{
                if(!empty($_POST)){
                    //agregar funcion comprobar dominio
                    if(!empty($dominio) &&!empty($marca) &&!empty($modelo) &&!empty($anio)){
                        $existe = $this->DAO_Vehiculos->traerVehiculo($dominio);
                        if($existe == null){
                            $v = new Vehiculo();
                            $v->set('dominio',$dominio);
                            $v->set('marca',$marca);
                            $v->set('modelo',$modelo);
                            $v->set('anio',$anio);
                            $this->DAO_Vehiculos->agregar($dni,$v);
                            $this->persona->set('vehiculos',$this->DAO_Vehiculos->traerVehiculos($_SESSION['dni']));
                            require "Views/vehiculos.view.php";
                        }else{
                            $alert = "Dominio ya ingresado.";
                            require "Views/agregarVehiculos.view.php";
                        }
                    }else{
                        $alert = "Ingrese todos los campos con (*)";
                        require "Views/agregarVehiculos.view.php";
                    }
                }else{
                    require "Views/agregarVehiculos.view.php";
                }
            }
        }else{
            Funciones::redireccionar();
            //                    Funciones::mostrarTodo("hola");
        }
    }
    /**
    *
    * si el boton pasado por parametro es igual movimientos redirige al MovimientosController con el dominio pasado por parametro
    * si es eliminar elimina de la BD el dominio pasado,
    * si es modificar, redirige a la vista de modificar el vehiculo con todos los datos traidos de la BD
    * @param $dominio string
    * @param $button Aux
    *
    */
    public function accion($dominio ="",$button=""){
        //        Funciones::mostrarTodo($_POST);
        Funciones::comprobarSesion();
        $dni = $_SESSION['dni'];

        if(!empty($dni)){
            $this->persona = $persona = $this->DAO_Persona->traerPersonaPorDni($dni);
            if(!isset($persona)){
                $_SESSION['dni'] = null;
                $alert = "No se encontro el DNI.";
                require "Views/admin.view.php";
            }else{
                if(!empty($_POST)){
                    $_SESSION['dominio'] = $dominio;
                    if($button == "modificar"){
                        $vehiculo = $this->DAO_Vehiculos->traerVehiculo($dominio);
                        require "Views/modificarVehiculo.view.php";
                    }else if($button == "movimientos"){
                        $location = "Location: " . ROOTFOLDERS . "MovimientoController/TraerDominio/$dominio";
                        header($location);
                    }else if($button == "eliminar"){
                        $this->DAO_Vehiculos->eliminarVehiculo($dominio);
                        //        Funciones::mostrarTodo("hola");
                        $location = "Location: " . ROOTFOLDERS . "VehiculoController/vehiculos";
                        header($location);
                    }
                }else{
                    require "Views/agregarVehiculos.view.php";
                }
            }
        }else{
            Funciones::redireccionar();
        }
    }
    /**
    *
    * valida todos los parametros y persiste los cambios en la BD
    * @param $marca string
    * @param $modelo string
    * @param $anio $string
    *
    */
    public function modificar($marca ="",$modelo ="",$anio =""){
        Funciones::comprobarSesion();
        $dominio = $_SESSION['dominio'];
        if(!empty($dominio)){
            if(!empty($dominio) &&!empty($marca) &&!empty($modelo) &&!empty($anio)){
                $vehiculo = new Vehiculo();
                $vehiculo->set('dominio',$dominio);
                $vehiculo->set('marca',$marca);
                $vehiculo->set('modelo',$modelo);
                $vehiculo->set('anio',$anio);

                $this->DAO_Vehiculos->modificar($vehiculo);

                $location = "Location: " . ROOTFOLDERS . "VehiculoController/vehiculos";
                header($location);
            }else{
                $this->persona = $persona = $this->DAO_Persona->traerPersonaPorDni($_SESSION['dni']);
                $vehiculo = $this->DAO_Vehiculos->traerVehiculo($dominio);
                $alert = "Ingrese todos los campos con (*)";
                require "Views/modificarVehiculo.view.php";
            }
        }else{
            Funciones::redireccionar();
        }
    }
}
