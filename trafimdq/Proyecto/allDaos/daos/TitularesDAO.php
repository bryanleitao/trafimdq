<?php namespace allDaos\daos;
use config\Constantes;
use config\Conexion;
use allDaos\idaos\IPdoDAO;
use allDaos\daos\VehiculosDAO;
use modelos\Titular;
use PDO;

class TitularesDAO extends Conexion implements IPdoDAO 
{
	protected $vehiculos;
	protected $tabla = "titulares";
	private static $instance = null;

	public static function getInstance() {
		if (self::$instance === NULL) {
			self::$instance = new self();
			}
		return self::$instance;
	}

	private function autoBind($params, $sentencia) {
		foreach ($params as $key => &$val) {
			$sentencia->bindParam(':'. $key, $val);
		}
		return $sentencia;
	}

	public function agregar($value) {
		$sql = "INSERT INTO " . $this->tabla . "(usuario, nombre, apellido, email, password, dni, fecha, direccion, tipo) 
		VALUES (:usuario, :nombre, :apellido, :email, :password, :dni, :fecha, :direccion, :tipo)";
		$parametros = $value->toArray(array('id','listaV'));
		$this->execPDO($sql, $parametros);
	}

	public function validarUsuario($value) {
		$sql = "SELECT usuario FROM " . $this->tabla . " WHERE usuario = " .'\''. $value .'\'';
		$sentencia = $this->execPdo($sql);
		$exists    = $sentencia->rowCount();
		return $exists;
	} 

	public function todos() {
		$sql 	   = "SELECT * FROM " . $this->tabla;
		$sentencia = $this->execPDO($sql);
		$array = [];
		$listado = NULL;
		while($row   = $sentencia->fetch()) {
			$array[] = $row;
			$listado = $this->mapear($array);	
		}
		$listado = $this->setVehiculo($listado,$array);
		return $listado;	
	}

	private function setVehiculo($listado,$array) {
		$this->vehiculos = VehiculosDAO::getInstance();
		foreach($listado as $titular) {			
			$var 	= array_shift($array);
			$vehiculo = $this->vehiculos->traerPorTit($var['id_titular']);
			$titular->setListaV($vehiculo);					
		}
		return $listado;
	}
	
	public function mapear($value) { 
		$value = array_map(function($p){	
			return new Titular($p['usuario'], $p['id_titular'], $p['nombre'], $p['apellido'], $p['email'], $p['password'], $p['dni'], $p['fecha'], $p['direccion'], $p['tipo']);
			}, $value); 
		return $value;
	}
	
	public function traerUno($value) {
		$sql 	   = "SELECT * FROM " . $this->tabla ." WHERE id_titular = " . $value;
		$sentencia = $this->execPdo($sql);
		$array[]   = $sentencia->fetch();
		$titular   = $this->mapear($array);
		$titular   = array_shift($titular);
		return $titular;
	}

	public function actualizar($value) {
		$sql = "UPDATE " . $this->tabla . " SET nombre = :nombre, apellido = :apellido, email = :email, password = :password, dni = :dni, fecha = :fecha, direccion = :direccion, tipo = :tipo WHERE id_titular = " . $value->getId();
		$parametros = $value->toArray(array('id','listaV','usuario'));
		$this->execPdo($sql, $parametros);
	}

	public function eliminar($value) {
		$sql = "DELETE FROM " . $this->tabla . " WHERE id_titular = :id_titular";
		$parametros['id_titular'] = $value;
		$this->execPdo($sql,$parametros);
	}

	private function execPDO($sql, $parametros = "") {
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		if(empty($parametros)) {
			$sentencia->execute();
		}
		else {
			$sentencia  = $this->autoBind($parametros, $sentencia);
			$sentencia->execute($parametros);
		}
		return $sentencia;
	}
}

