-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2016 a las 00:54:06
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sarroden`
--
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `saldo` int(11) NOT NULL,
  `infraccion` int(11) NOT NULL,
  `id_cuenta` int(11) NOT NULL,
  `peaje` int(11) NOT NULL,
  `pago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`saldo`, `infraccion`, `id_cuenta`, `peaje`, `pago`) VALUES
(1250, -2400, 38, 0, 3350),
(0, -1500, 39, 0, 1500),
(-50, -150, 43, 0, 100),
(0, -450, 44, 0, 450),
(0, 0, 45, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `titulares`
--

CREATE TABLE `titulares` (
  `id_titular` int(11) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `dni` varchar(15) NOT NULL,
  `direccion` varchar(20) NOT NULL,
  `fecha` date DEFAULT NULL,
  `tipo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `titulares`
--

INSERT INTO `titulares` (`id_titular`, `usuario`, `nombre`, `apellido`, `email`, `password`, `dni`, `direccion`, `fecha`, `tipo`) VALUES
(21, 'i', 'i', 'i', 'i', 'i', 'i', 'i', '0000-00-00', 1),
(30, 'Amargas', 'Juan Carlos', 'Pelotas Amargas', 'pamargas@mailinator.com', '123', '23456789', 'Paso 4433', '1979-01-01', 1),
(31, 'JonSnow', 'Jon', 'Snow', 'jon.snow@gmail.com', 'sansa', '10234901', 'winterfell 26', '1960-11-11', 0),
(32, 'Dante', 'Dante', 'Dante', 'Dante', 'Dante', 'Dante', 'Dante', '0000-00-00', 1),
(33, 'Facu', 'Facu', 'Malgieri', 'aaa@aaa', 'Facu', '123', 'ooo 111', '0000-00-00', 0),
(34, 'osqui', 'o', 'o', 'o', 'o', 'o', 'o', '0000-00-00', 0),
(35, 'o', 'o', 'o', 'o', 'o', 'o', 'o', '0000-00-00', 0),
(36, 'a', 'i', 'i', 'i', 'i', 'i', 'i', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id_vehiculo` int(11) NOT NULL,
  `id_titular_fk` int(11) DEFAULT NULL,
  `dominio` varchar(10) NOT NULL,
  `marca` varchar(30) NOT NULL,
  `modelo` varchar(10) NOT NULL,
  `qr` varchar(10) NOT NULL,
  `id_cuenta_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id_vehiculo`, `id_titular_fk`, `dominio`, `marca`, `modelo`, `qr`, `id_cuenta_fk`) VALUES
(22, 21, 'fhj 123', 'Toyota', 'auto1', '1', 38),
(23, 21, 'sql 123', 'Renault', 'auto2', '1', 39),
(24, 31, 'ppp 123', 'Audi', 'tt', '1', 43),
(25, 31, 'yyy', 'Honda', 'Civic', '1', 44),
(26, 36, 'prueba 123', 'Ford', 'forro', '1', 45);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id_cuenta`);

--
-- Indices de la tabla `titulares`
--
ALTER TABLE `titulares`
  ADD PRIMARY KEY (`id_titular`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id_vehiculo`),
  ADD KEY `id_titular_fk` (`id_titular_fk`),
  ADD KEY `id_cuenta_fk` (`id_cuenta_fk`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id_cuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `titulares`
--
ALTER TABLE `titulares`
  MODIFY `id_titular` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id_vehiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `Vehiculos_ibfk_1` 
    FOREIGN KEY (`id_titular_fk`) 
    REFERENCES `titulares` (`id_titular`) 
  ON DELETE CASCADE;

ALTER table cuentas add id_vehiculo_fk int,
    add CONSTRAINT FOREIGN KEY (`id_vehiculo_fk`) 
    REFERENCES `vehiculos` (`id_vehiculo`)
  ON DELETE CASCADE
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
