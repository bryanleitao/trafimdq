<?php namespace Models;

use Config\Funciones;

abstract class ClaseConvertibleAArray {

    // El parametro atributosExcluidos es un array de aquellos atributos que NO quiero transformar
    public function toArray($atributosNoConvertibles = null) {
        $resultado = array();

        // una magia que recorre todos los atributos de la clase como si fuera un array
        // tomo cada atributo y voy creando un array asociativo con clave-valor


        $verificarExcluidos = (!is_null($atributosNoConvertibles)) ? true : false;

//        Funciones::mostrarTodo($this);
        foreach ($this as $atributo => $valor) {

            if ($verificarExcluidos){
                if (!in_array($atributo, $atributosNoConvertibles))
                    $resultado[$atributo] = $valor;
            }else
                $resultado[$atributo] = $valor;
        }

        return $resultado;
    }

}
