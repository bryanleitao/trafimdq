<?php namespace Daos;

use Daos\ICuentaDAO;
use Daos\conexion;
use PDO;
/**
 * Esta clase deberia ser SINGLETON sino no funcionaria correctamente
 * de la manera en que está programada. No crear más de una instancia.
 *
 * ¿Por qué?
 */
class CuentaMySqlDAO implements ICuentaDAO{
    protected $listado;
    private static $instance = null;

    public static function getInstance() {
        if (self::$instance === NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**
     * Constructor
     *
     * @param string $ruta Ruta del archivo "base de datos"
     */
    public function __construct(){

    }

    public function traerCuenta($dominio){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerCuentaPorDominio($dominio);";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetch(PDO::FETCH_ASSOC);

        if(!empty($stm)){
            $stm = $this->mapear($stm);
            return ($stm) ? $stm : false;
        }
    }

    public function mapear($m) {

        $movimiento  = new Movimiento();
        $movimiento->set('movimiento',$m['movimiento']);
        $movimiento->set('saldo',$m['saldo']);
        $movimiento->set('fecha',$m['fecha']);

        //        Funciones::mostrarTodo($movimiento);
        return $movimiento;

    }

}
