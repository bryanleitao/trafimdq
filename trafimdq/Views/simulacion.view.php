<div class="container mar-v-md">
    <!--                titular                 -->
    <h3 class="cl-dark">Datos del Vehiculo:</h3>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="panel panel-info">
                <form method="POST" id="formPersona" action="<?php echo ROOTFOLDERS ?>SimulacionController/simular" enctype="multipart/form-data">
                    <div class="row mar-v-sm mar-h-sm">
                        <div class="col-md-4">
                            <label for="#">Dominio (*)</label>
                            <select class="form-control" name="dominio" id="">
                                <?php foreach($vehiculos as $v):?>
                                <option value="<?php echo $v->get('dominio');?>"><?php echo $v->get('dominio'). " - " . $v->get('dni');?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="#">Movimiento multa/peaje/pago (*)</label>
                            <select name="movimiento" class="form-control">
                                <option value="peaje">Peaje</option>
                                <option value="multa">Multa</option>
                                <option value="pago">Pago</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="#">Importe (*)</label>
                            <input name="importe" type="text" class="form-control" placeholder="Importe $" value="">
                        </div>
                    </div>
                    <div class="row mar-h-sm">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <?php if(!empty($alert)):?>
                                <label class="alert-danger" for="error"><?= $alert; ?></label>
                                <?php endif;?>
                                <button type='submit' form='formPersona' class='btn btn-warning'>Simular</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->

