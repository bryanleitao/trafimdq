<?php namespace Controllers;

use Models\persona;
use Config\Funciones;
use Daos\PersonaMysqlDAO;

class AdminController{
//    private $DAO_Persona;
//    private $persona = null;
//    private $alert;

    public function __construct(){
        $this->DAO_Persona = PersonaMysqlDAO::getInstance();
    }

    public function index(){
        Funciones::comprobarSesion();
        $usuario = $_SESSION['usuario'];
        $_SESSION = null;
        $_SESSION['usuario'] = $usuario;
        require "Views/admin.view.php";

        //                Funciones::mostrarTodo($_SESSION['usuario']);
    }
/*
*
* pasa el dni traido por parametro a TitularController y le pasa la responsabilidad de mostrar a ese controlador
* @param $dni int
*
*/
    public function buscar($dni =""){
        Funciones::comprobarSesion();

        if(!empty($dni)){
            $_SESSION['dni'] = $dni;
            $location = "Location: " . ROOTFOLDERS . "TitularController/buscar/$dni";
            header($location);
        }else{
            $alert = "Ingrese un DNI.";
            require "Views/admin.view.php";
        }

        //        Funciones::mostrarTodo($this->persona);
    }
}



