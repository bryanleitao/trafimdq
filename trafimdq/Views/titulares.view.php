<div class="container mar-v-md">
    <!--                Movimientos                -->
    <h3 class="cl-dark">Titulares <i class="fa fa-angle-right"></i></h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="row mar-v-sm mar-h-sm">
                    <div class="col-md-12">
                        <table class="table table-hover table-responsive" id="tableTitulares">
                            <thead>
                                <tr>
                                    <th>Foto</th>
                                    <th>Nombre y Apellido</th>
                                    <th colspan="2">DNI</th>
                                    <th>Direccion</th>
                                    <th>Telefono</th>
                                    <th>Fecha de nacimiento</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Movimientos</th>
                                    <th>Perfil</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($titulares as $t):?>
                                <tr>
                                    <td><img class="img-circle center-block" src="<?php echo URL_IMG . "titulares/" . $t['foto'];?>"></td>
                                    <td><?= $t['nomb_ape']?></td>
                                    <td><a href="<?php echo ROOTFOLDERS . "TitularController/buscar/" . $t['dni']?>"><?= $t['dni'] ?></a></td>
                                    <td><a href="<?php echo ROOTFOLDERS . "TitularController/editar/" . $t['dni']?>"><i class="fa fa-pencil"></i></a></td>
                                    <td><?= $t['domicilio'] ?></td>
                                    <td><?= $t['telefono'] ?></td>
                                    <td><?= $t['fecha_nacimiento'] ?></td>
                                    <td><?= $t['email'] ?></td>
                                    <td><button type="button" class="btn btn-inverse"><i class="fa fa-repeat"></i></button></td>
                                    <td><a href="<?php echo ROOTFOLDERS . "MovimientoController/TraerMovimientos/" . $t['dni']?>"><i class="fa fa-list"></i></a></td>
                                    <td><?= $t['rol'] ?></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->
