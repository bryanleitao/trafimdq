<div class="container mar-v-md">
    <!--                Automotor                -->
    <h3 class="cl-dark"><a href="<?php echo ROOTFOLDERS ?>TitularController/">Titular</a> <i class="fa fa-angle-right"></i> <a href="<?php echo ROOTFOLDERS ?>VehiculoController/vehiculos">Vehiculos</a> <i class="fa fa-angle-right"></i>  Agregar</h3>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="panel panel-info">
                <form method="POST" id="formVehiculo" action="<?= ROOTFOLDERS ?>VehiculoController/agregar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-heading">
                                <?php if(isset($persona)):?>
                                <h4 class="pull-right">Fecha de alta:
                                    <?php echo $persona->get('fecha_alta');?>
                                </h4>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="row mar-v-sm mar-h-sm">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12">
                                    <output id="list">
                                        <?php if(isset($persona)):?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/" . $persona->get('foto');?>">
                                        <?php else: ?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/imgdefault.png"?>">
                                        <?php endif; ?>
                                    </output>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get('nombre');?> <?php if(isset($persona)) echo $persona->get("apellido");?></label>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get("domicilio");?></label>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get("telefono");?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <label for="#">Dominio (*)</label>
                            <input name="dominio" type="text" class="form-control" placeholder="Dominio" value="">
                            <label for="#">Marca (*)</label>
                            <input name="marca" type="text" class="form-control" placeholder="Marca" value="">
                            <label for="#">Modelo (*)</label>
                            <input name="modeo" type="text" class="form-control" placeholder="Modelo" value="">
                            <label for="#">Año (*)</label>
                            <input name="anio" type="text" class="form-control" placeholder="Año" value="">
                        </div>
                    </div>
                    <div class="row mar-h-sm">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                            <?php if(!empty($alert)):?>
                            <label class="alert-danger" for="error"><?= $alert; ?></label>
                            <?php endif;?>
                                <button type='submit' value='crear' name='crear' form='formVehiculo' class='btn btn-warning'>Crear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
