<?php namespace Models;

use JsonSerializable;

class Cuenta extends ClaseConvertibleAArray implements JsonSerializable{

    protected $movimientos;//
    protected $saldo;//

    public function __construct(){

    }

    public function JsonSerialize(){
        $this->toArray();
    }
    public function get($atributo){
        return $this->$atributo;
    }
    public function set($atributo,$valor){
        $this->$atributo = $valor;
    }
}
