<?php namespace allDaos\idaos;

	interface IPdoDAO 
	{
		public function agregar($value);

		public function todos();

		public function traerUno($value);

		public function eliminar($value);

		public function actualizar($value);		

	}

