</div>

<footer class="mar-v-lg ">
    <div class="container">
        <div class="row pad-v-md ">
            <div class="col-md-6">
                <h3>Municipalidad de Gral. Pueyrredon</h3>
                <hr>
                <p> Municipalidad de General Pueyrredon | Hipólito Yrigoyen 1627 | Mar del Plata | Buenos Aires | Argentina |
                    <abbr title="Telefono">T:</abbr>
                    +54 223 499 6500 </p>
            </div>
            <div class="col-md-offset-3 col-md-3">
                <div class="span">
                    <div class="icon"></div>
                </div>
                <hr>
                <div class="text-center">
                    <a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row pad-v-sm">
            <div class="col-md-12">
                © 2016 Sarasa S.A. | Todos los derechos reservados<span class="pull-right">Diseño y Maquetación Web</span>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>-->
<script type="text/javascript" src="<?php echo URL_JS ?>angular.min.js"></script>
<script type="text/javascript" src="<?php echo URL_JS ?>app.js"></script>
<script src="<?php echo URL_JS ?>jquery-1.8.1.min.js"></script>
<script src="<?php echo URL_JS ?>scripts.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){

        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.top').fadeIn();
            } else {
                $('.top').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.top').click(function(){
            $('html, body').animate({scrollTop : 0},1500);
            return false;
        });

    });
</script>
<style type="text/css">
    .top {
        height:50px;
        width:50px;
        text-decoration: none;
        position:fixed;
        top:550px;
        right:40px;
        display:none;
        /*                background: #000;*/
        /*                color: #fff;*/
        /*                background-image:url(to-top.png);*/
        background-repeat:no-repeat;
    }
    .top:hover {
        /*                color: #fff;*/
        /*                border:1px #CCC solid;*/
        text-decoration:none;
    }

</style>
<script type="text/javascript">
    //javascript:(function(){var s=document.createElement("script");s.onload=function(){bootlint.showLintReportForCurrentDocument([]);};s.src="https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js";document.body.appendChild(s)})();
</script>
</body>

</html>


