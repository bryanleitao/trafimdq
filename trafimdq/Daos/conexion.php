<?php namespace Daos;

use PDO;

class Conexion{

    public function conectar(){
        $DB_OPC = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
        $c = new PDO("mysql:host=" . DB_SERVER . "; dbname=" . DB_NAME, DB_USER, DB_PASS, $DB_OPC);
        return $c;
    }
}

