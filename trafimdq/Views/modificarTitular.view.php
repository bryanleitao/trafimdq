<div class="container mar-v-md">
    <h3 class="cl-dark"><a href="<?php echo ROOTFOLDERS ?>TitularController/">Titular</a> <i class="fa fa-angle-right"></i> Modificar</h3>
    <!--                titular                 -->
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="panel panel-info">
                <form method="POST" id="formPersona" action="<?= ROOTFOLDERS ?>TitularController/modificar" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-heading">
                                <?php if(isset($persona)):?>
                                <h4 class="pull-right">Fecha de alta:
                                    <?php echo $persona->get('fecha_alta');?>
                                </h4>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="row mar-v-sm mar-h-sm">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12">
                                    <output id="list">
                                        <?php if(isset($persona)):?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/" . $persona->get('foto');?>">
                                        <?php else: ?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/imgdefault.png"?>">
                                        <?php endif; ?>
                                    </output>
                                </div>
                                <div class="col-md-12 mar-v-sm">
                                    <div class="btn btn-default btn-more-contact" data-toggle="tooltip" data-placement="left" title="Cargar mas contactos">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        Subir foto
                                        <input name="foto" id="files" type="file" class="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <input type="hidden" name="imgAux" value="<?php echo $persona->get('foto');?>">
                            <label for="#">Nombre(*)</label>
                            <input name="nombre" type="text"class="form-control" placeholder="Nombre" value="<?php if(isset($persona)) echo $persona->get('nombre');?>">
                            <label for="#">Apellido(*)</label>
                            <input name="apellido" type="text" class="form-control" placeholder="Apellido" value="<?php if(isset($persona)) echo $persona->get("apellido");?>">
                            <label for="#">Dni</label>
                            <fieldset disabled>
                                <input type="text" class="form-control" placeholder="Dni" value="<?php if(isset($persona)) echo $persona->get('dni');?>">
                            </fieldset>
                            <label for="#">Direccion(*)</label>
                            <input name="direccion" type="text" class="form-control" placeholder="Direccion" value="<?php if(isset($persona)) echo $persona->get("domicilio");?>">
                            <label for="#">Fecha de nacimiento(*)</label>
                            <input name="fecha_nac" type="text" class="form-control" placeholder="dia-mes-año" value="<?php if(isset($persona)) echo $persona->get("fecha_nacimiento");?>">
                            <label for="#">Email</label>
                            <input name="email" type="text" class="form-control" placeholder="Email" value="<?php if(isset($persona)) echo $persona->get('email');?>">
                            <label for="#">Telefono(*)</label>
                            <input name="telefono" type="text" class="form-control" placeholder="Telefono" value="<?php if(isset($persona)) echo $persona->get("telefono");?>">
                            <div class="form-group">
                                <label for="sel1">Perfil</label>
                                <select class="form-control" name="perfil">
                                    <option value="1"<?php if(isset($persona)) if($persona->get('perfil') == "Administrador")echo "selected";?>>Administrador</option>
                                    <option value="2"<?php if(isset($persona)) if($persona->get('perfil') == "Empleado")echo "selected";?>>Empleado</option>
                                    <option value="3"<?php if(isset($persona)) if($persona->get('perfil') == "Titular")echo "selected";?>>Titular</option>
                                </select>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estado" id="rad1" value="1" <?php if(isset($persona) && ($persona->get('estado') == '1')) echo 'checked'; ?>>
                                        Activo
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estado" id="rad2" value="2" <?php if(isset($persona) && ($persona->get('estado') == '2')) echo 'checked'; ?>>
                                        Inactivo
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mar-h-sm">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <?php if(!empty($alert)):?>
                                <label class="alert-danger" for="error"><?= $alert; ?></label>
                                <?php endif;?>
                                <button type='submit' value='modificar' name='modificar' form='formPersona' class='btn btn-warning'>Modificar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--                /titular                 -->
</div>

