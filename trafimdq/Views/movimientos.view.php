<div class="container mar-v-md">
    <!--                Movimientos                -->
    <h3 class="cl-dark"><a href="<?php echo ROOTFOLDERS ?>TitularController/">Titular</a> <i class="fa fa-angle-right"></i> <a href="<?php echo ROOTFOLDERS ?>VehiculoController/vehiculos">Vehiculos</a> <i class="fa fa-angle-right"></i> Movimientos</h3>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="panel panel-info">
                <form method="POST" id="formVehiculo" action="<?= ROOTFOLDERS ?>VehiculoController/accion">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-heading">
                                <?php if(isset($persona)):?>
                                <h4 class="pull-right">Fecha de alta:
                                    <?php echo $persona->get('fecha_alta');?>
                                </h4>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="row mar-v-sm mar-h-sm">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12">
                                    <output id="list">
                                        <?php if(isset($persona)):?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/" . $persona->get('foto');?>">
                                        <?php else: ?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/imgdefault.png"?>">
                                        <?php endif; ?>
                                    </output>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get('nombre');?> <?php if(isset($persona)) echo $persona->get("apellido");?></label>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get("domicilio");?></label>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get("telefono");?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Dominio</th>
                                        <th>Movimiento</th>
                                        <th>Importe</th>
                                        <th>Fecha y Hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($movimientos)):foreach($movimientos as $m): ?>
                                    <tr>
                                        <td><a href="<?php echo ROOTFOLDERS ?>MovimientoController/TraerDominio/<?= $m['dominio']?>"><?= $m['dominio']?></a></td>
                                        <td><?= $m['movimiento']?></td>
                                        <td><?= $m['importe']?></td>
                                        <td><?php $m['fecha'] = new datetime($m['fecha']); echo $m['fecha']->format('d-m-Y H:i:s');?></td>
                                    </tr>
                                    <?php endforeach;endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->

