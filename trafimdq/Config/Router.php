<?php namespace Config;

class Router{

    /**
     * @param Request $request
     */
    public static function run(Request $request) {
        $controlador = $request->get("controlador");
        $metodo = $request->get("metodo");
        $parametros = $request->get("parametros");

        $ruta = ROOT . 'Controllers' . DS . $controlador . '.php';

        if($metodo == "index.php"){
            $metodo = "index";
        }

        if(is_readable($ruta)){
            require_once $ruta;
            $mostrar = "Controllers". DS . $controlador;
            $controlador = new $mostrar;

            if(!isset($parametros)) {
                call_user_func(array($controlador, $metodo));
            } else {
                call_user_func_array(array($controlador, $metodo), $parametros);
            }
        }
    }
}


        //        $controlador = $request->getControlador() . 'Controlador';
        //        $metodo = $request->getMetodo();
        //        $parametros = $request->getParametros();
