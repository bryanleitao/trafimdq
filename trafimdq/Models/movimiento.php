<?php namespace Models;

use JsonSerializable;

class Movimiento extends ClaseConvertibleAArray implements JsonSerializable{

    protected $movimiento;//
    protected $importe;//
    protected $fecha;//

    public function __construct(){

    }

    public function JsonSerialize() {
        $this->toArray();
    }
    public function get($atributo){
        return $this->$atributo;
    }
    public function set($atributo,$valor){
        $this->$atributo = $valor;
    }
}
