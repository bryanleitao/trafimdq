<?php namespace Controllers;

use Config\Funciones;
use Models\vehiculo;
use Models\movimiento;
use Daos\VehiculoMySqlDAO;
use Daos\MovimientoMySqlDAO;

class SimulacionController{
    private $alert = "";
    public function __construct(){
        $this->DAO_Vehiculos = VehiculoMySqlDAO::getInstance();
        $this->DAO_Movimientos = MovimientoMySqlDAO::getInstance();
    }
    /*
    * muestra la vista del simulador de movimientos
    *
    */
    public function index(){
        Funciones::comprobarSesion();
        if($_SESSION['usuario']->get('perfil') == "Administrador"){
            $vehiculos = $this->DAO_Vehiculos->traerListado();
            $alert = $this->alert;
            require "Views/simulacion.view.php";
        }else{
            Funciones::redireccionar();
        }
    }
    /*
    * lanza un movimiento a un dominio determinado
    * @param $dominio string
    * @param $movimiento string nombre del movimiento
    * @param $importe float
    *
    */
    public function simular($dominio ="",$movimiento ="",$importe =""){
        Funciones::comprobarSesion();
//            Funciones::mostrarTodo($_SESSION['usuario']->get('perfil') == "Administrador");
        if($_SESSION['usuario']->get('perfil') == "Administrador"){
            if(!empty($dominio) && !empty($movimiento) && !empty($importe)){
                $m = new Movimiento();
                $m->set('movimiento',$movimiento);
                $m->set('importe',$importe);
                $v = new Vehiculo();
                $v->set('dominio',$dominio);
                $this->DAO_Movimientos->simular($v,$m);
                $location = "Location: " . ROOTFOLDERS . "SimulacionController/";
                header($location);
            }else{
                $this->alert .= "Ingrese todos los campos";
                $this->index();
            }
        }else{
            Funciones::redireccionar();
        }
    }


}
