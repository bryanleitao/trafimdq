<?php namespace allDaos\daos;
use config\Constantes;
use config\Conexion;
use allDaos\idaos\IPdoDAO;
use modelos\Cuenta;
use PDO;

class CuentasDAO extends Conexion implements IPdoDAO 
{	
	protected $tabla = "cuentas";
	private static $instance = null;

	public static function getInstance() {
		if (self::$instance === NULL) {
			self::$instance  = new self();
		}
		return self::$instance;
	}

	public function autoBind($params, $sentencia) {
		foreach ($params as $key => &$val) {
			$sentencia->bindParam(':'. $key, $val);
		}
		return $sentencia;
	}

	public function agregar($value) {
		$sql = "INSERT INTO " . $this->tabla . "(saldo, infraccion, peaje, pago) VALUES (:saldo, :infraccion, :peaje, :pago)";
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$parametros = $value->toArray(array('id_cuenta'));
		$sentencia  = $this->autoBind($parametros, $sentencia);
		$sentencia->execute($parametros);
		$id 	= $conexion->lastInsertId();
		$cuenta = $this->traerUno($id);
		return $cuenta;	
	}

	public function todos() {
		$sql = "SELECT * FROM " . $this->tabla;
		$obj_pdo = new Conexion();
		$sentencia = $this->execPdo($sql);

		while ($row = $sentencia->fetch()) {
			$array[] = $row;
			$listado = $this->mapear($array);
		}
		return $listado;
	}

	public function traerUno($value){
		$sql = "SELECT * FROM " . $this->tabla ." WHERE id_cuenta = " . $value;
		$sentencia = $this->execPdo($sql);
		$cuenta[] = $sentencia->fetch();
		$cuentaObjeto = $this->mapear($cuenta);
		$cuentaObjeto = array_shift($cuentaObjeto);
		return $cuentaObjeto;
	}

	public function registrarPago($cuenta) {
		$sql = "UPDATE " . $this->tabla . " SET pago = :pago, saldo = :saldo WHERE id_cuenta = :id_cuenta";
		$parametros = $cuenta->toArray(array('infraccion','peaje'));
		$this->execPdo($sql,$parametros);
	}

	public function registrarInfraccion($cuenta) {
		$sql = "UPDATE " . $this->tabla . " SET infraccion = :infraccion, saldo = :saldo WHERE id_cuenta = :id_cuenta";
		$parametros = $cuenta->toArray(array('pago','peaje'));
		$this->execPdo($sql,$parametros);
	}

	public function registrarPeaje($cuenta) {
		$sql = "UPDATE " . $this->tabla . " SET peaje = :peaje, saldo = :saldo WHERE id_cuenta = :id_cuenta";
		$parametros = $cuenta->toArray(array('pago','infraccion'));
		$this->execPdo($sql,$parametros);
	}
	
	public function vincular($cuenta, $id) {
		$sql = "UPDATE " . $this->tabla . " SET id_vehiculo_fk = " . $id . " WHERE id_cuenta = " . $cuenta->getId();
		$this->execPdo($sql);
	}

	public function mapear($value) { 
		$value = array_map(function($p){	
			return new Cuenta($p['id_cuenta'], $p['saldo'], $p['infraccion'], $p['peaje'], $p['pago']);
			}, $value); 
		return $value;
	}

	private function execPDO($sql,$parametros = "") {
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		if(empty($parametros)) {
			$sentencia->execute();
		}
		else {
			$sentencia  = $this->autoBind($parametros, $sentencia);
			$sentencia->execute($parametros);
		}
		return $sentencia;
	}

	public function eliminar($value) {
		// No es necesario implementarlo por ahora, las cuentas se borran on cascade en relacion al vehiculo.
	}	
	public function actualizar($value) {
		// No es necesario implementarlo por ahora
	}	
}


/*public function getUltimoId(){
		$sql = "SELECT MAX(id_cuenta) as id FROM " . $this->tabla;
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		$lastId = $sentencia->fetch();
		$id = $lastId['id'];
		return $id;
	}
*/