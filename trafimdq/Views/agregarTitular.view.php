   <div class="container mar-v-md">
    <h3 class="cl-dark">Titular <i class="fa fa-angle-right"></i> Agregar</h3>
    <!--                titular                 -->
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="panel panel-info">
                <form method="POST" id="formPersona" action="<?= ROOTFOLDERS ?>TitularController/agregar" enctype="multipart/form-data">
                    <div class="row mar-v-sm mar-h-sm">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12">
                                    <output id="list">
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/imgdefault.png"?>">
                                    </output>
                                </div>
                                <div class="col-md-12 mar-v-sm">
                                    <div class="btn btn-default btn-more-contact" data-toggle="tooltip" data-placement="left" title="Cargar mas contactos">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        Subir foto
                                        <input name="foto" id="files" type="file" class="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <label for="#">Nombre (*)</label>
                            <input name="nombre" type="text"class="form-control" placeholder="Nombre" value="">
                            <label for="#">Apellido (*)</label>
                            <input name="apellido" type="text" class="form-control" placeholder="Apellido" value="">
                            <label for="#">Dni (*)</label>
                            <input name="dni" type="text" class="form-control" placeholder="Dni" value="">
                            <label for="#">Direccion (*)</label>
                            <input name="direccion" type="text" class="form-control" placeholder="Direccion" value="">
                            <label for="#">Fecha de nacimiento (*)</label>
                            <input name="fecha_nac" type="text" class="form-control" placeholder="dia-mes-año" value="">
                            <label for="#">Email</label>
                            <input name="email" type="text" class="form-control" placeholder="Email" value="">
                            <label for="#">Telefono (*)</label>
                            <input name="telefono" type="text" class="form-control" placeholder="Telefono" value="">
                            <div class="form-group">
                                <label for="sel1">Perfil</label>
                                <select class="form-control" name="perfil">
                                    <option value="1">Administrador</option>
                                    <option value="2">Empleado</option>
                                    <option value="3">Titular</option>
                                </select>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estado" id="rad1" value="1" >
                                        Activo
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="estado" id="rad2" value="2" checked>
                                        Inactivo
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mar-h-sm">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <?php if(!empty($alert)):?>
                                <label class="alert-danger" for="error"><?= $alert; ?></label>
                                <?php endif;?>
                                <button type='submit' value='crear' name='crear' form='formPersona' class='btn btn-warning'>Crear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--                /titular                 -->
</div>

