<?php namespace Controllers;

use Models\movimiento;
//use Daos\TitularJsonDAO;
use Daos\MovimientoMySqlDAO;
use Daos\PersonaMySqlDAO;
use Config\Funciones;

class MovimientoController{
    //    private $DAO_Movimientos;
    //    private $DAO_Persona;

    public function __construct(){
        $this->DAO_Movimientos = MovimientoMysqlDAO::getInstance();
        $this->DAO_Persona = PersonaMysqlDAO::getInstance();
    }
    /*
    * trae los movimientos de un dominio
    * @param $dominio string
    *
    */
    public function traerDominio($dominio =""){
//        Funciones::mostrarTodo($dominio);
        if(!empty($_SESSION['dni'])){
            $persona = $this->DAO_Persona->traerPersonaPorDni($_SESSION['dni']);

            if($this->DAO_Movimientos->comprobarDominio($_SESSION['dni'],$dominio)){
                $movimientos = $this->DAO_Movimientos->traerMovimientoPorDominio($dominio);
                require "Views/movimientos.view.php";
            }else{
                Funciones::redireccionar();
            }
        }else{
            Funciones::redireccionar();
        }
    }
    /*
    * trae los movimientos todos los dominios de un titular
    *
    */
    public function traerMovimientos($dni=""){
        if(!empty($dni)){
            $_SESSION['dni'] = $dni;
            $persona = $this->DAO_Persona->traerPersonaPorDni($dni);
            $movimientos = $this->DAO_Movimientos->traerListado($dni);
            require "Views/movimientos.view.php";
        }else{
            Funciones::redireccionar();
        }
    }


}
