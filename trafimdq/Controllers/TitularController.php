<?php namespace Controllers;

use Models\persona;
use Config\Funciones;
use Daos\PersonaMysqlDAO;


class TitularController{
    private $DAO_Persona;
    private $persona;

    public function __construct(){
        $this->DAO_Persona = PersonaMysqlDAO::getInstance();
    }


    /**
    *
    * muestra a partir del dni, si es que lo encuentra en la variable de sesion,
    * los datos del titular, de lo contrario se redirige al AdminController/index
    *
    */
    public function index(){
        Funciones::comprobarSesion();

        if(!empty($_SESSION['dni'])){
            $location = "Location: " . ROOTFOLDERS . "TitularController/buscar/{$_SESSION['dni']}";
            header($location);}
        else{
            Funciones::redireccionar();
        }
    }
    /**
    *
    * valida el titular pasado por parametro y trae todos los datos de dicho titular
    * @param int $dni
    *
    */
    public function buscar($dni = ""){
        Funciones::comprobarSesion();

        if(!empty($dni)){
            $persona = $this->DAO_Persona->traerPersonaPorDni($dni);

            if(!isset($persona)){
                $_SESSION['dni'] = null;
                $alert = "No se encontro el DNI.";
                require "Views/admin.view.php";
            }else{
                $_SESSION['dni'] = $dni;
                require "Views/titular.view.php";
            }
        }else{
            Funciones::redireccionar();
        }
    }
    /**
    *
    * muestra a partir del dni, si es que lo encuentra en la variable de sesion,
    * los datos del titular y lo redirige en el view de modificar el titular, de lo contrario se redirige al AdminController/index
    *
    */
    public function editar($dni=""){
        Funciones::comprobarSesion();

        if(!empty($dni)){
            $_SESSION['dni'] = $dni;
            $this->persona = $persona = $this->DAO_Persona->traerPersonaPorDni($dni);
            require "Views/modificarTitular.view.php";
        }else{
//            Funciones::redireccionar();
        }
    }
    /**
    *
    * valida todos los datos pasados por parametro, de que no este vacios para poder guardar los cambios en la BD
    * @param $imgAux string, guardo el nombre de la imagen del titular actual
    * @param $nombre string
    * @param $apellido string
    * @param $domicilio string
    * @param $fecha_nac date
    * @param $email string
    * @param $telefono string
    * @param $perfil int
    * @param $estado int
    * @param $button aux
    * @param $foto $_FILE, en caso de que se cambie la foto, aca se guardan todos los datos nuevos
    *
    */
    public function modificar($imgAux ="",$nombre ="",$apellido ="",$domicilio ="",$fecha_nac ="",$email ="",$telefono ="",$perfil ="",$estado="",$button ="",$foto =""){
        Funciones::comprobarSesion();

        if(!empty($_SESSION['dni'])){
            $persona = $this->DAO_Persona->traerPersonaPorDni($_SESSION['dni']);

            if(isset($persona)){
                if(!empty($nombre) &&!empty($apellido) &&!empty($domicilio) &&!empty($fecha_nac) &&!empty($telefono)){
                    if(!empty($foto)){
                        if ($foto != $imgAux){
                            $file = $foto['tmp_name'];
                            $name = uniqid() . ".jpg";
                            $archivo_subido = ROOT . "Views/img/titulares/" . $name;
                            move_uploaded_file($file, $archivo_subido);
                            $foto = $name;
                        }
                    }else{
                        $foto = $imgAux;
                    }

                    $persona->set('nombre',$nombre);
                    $persona->set('apellido',$apellido);
                    $persona->set('domicilio',$domicilio);
                    $persona->set('fecha_nacimiento',str_replace("/","-", $fecha_nac));
                    $persona->set('email',$email);
                    $persona->set('telefono',$telefono);
                    $persona->set('perfil',$perfil);
                    $persona->set('estado',$estado);
                    $persona->set('foto',$foto);

                    $this->DAO_Persona->modificar($persona);

                    $location = "Location: " . ROOTFOLDERS . "TitularController/buscar/{$_SESSION['dni']}";
                    header($location);
                }else{
                    $persona = $this->DAO_Persona->traerPersonaPorDni($_SESSION['dni']);
                    $alert = "Ingrese todos los campos con (*)";
                    require "Views/modificarTitular.view.php";
                }
            }
        }else{
            Funciones::redireccionar();
        }
    }
    /**
    *
    * valida todos los datos pasados por parametro, de que no este vacios para poder persistir en la BD
    * @param $nombre string
    * @param $apellido string
    * @param $dni int
    * @param $domicilio string
    * @param $fecha_nac date
    * @param $email string
    * @param $telefono string
    * @param $perfil int
    * @param $estado int
    * @param $button aux
    * @param $foto $_FILE
    *
    */
    public function agregar($nombre ="",$apellido ="",$dni ="",$domicilio ="",$fecha_nac ="",$email ="",$telefono ="",$perfil ="",$estado="",$button ="",$foto =""){
        Funciones::comprobarSesion();


        if(!empty($_POST)){
            if(!empty($nombre) &&!empty($apellido) &&!empty($dni) &&!empty($domicilio) &&!empty($fecha_nac) &&!empty($telefono)){
                $existe = $this->DAO_Persona->traerPersonaPorDni($dni);
                //                    Funciones::mostrarTodo($existe);
                if($existe == null){
                    if(!empty($foto)){
                        $file = $foto['tmp_name'];
                        $name = uniqid() . ".jpg";
                        $archivo_subido = ROOT . "Views/img/titulares/" . $name;
                        move_uploaded_file($file, $archivo_subido);
                        $foto = $name;
                    }else{
                        $foto = IMGDEFAULT;
                    }
                    $password = $dni;

                    $p = new Persona();
                    $p->set('password',$password);
                    $p->set('email',$email);
                    $p->set('perfil',$perfil);
                    $p->set('dni',$dni);
                    $p->set('nombre',$nombre);
                    $p->set('domicilio',$domicilio);
                    $p->set('telefono',$telefono);
                    $p->set('foto',$foto);
                    $p->set('fecha_nacimiento',str_replace("/","-", $fecha_nac));
                    $p->set('apellido',$apellido);
                    $p->set('estado',$estado);

                    $this->DAO_Persona->agregar($p);
                    $_SESSION['dni'] = $dni;
                    $location = "Location: " . ROOTFOLDERS . "VehiculoController/agregar";
                    header($location);
                }else{
                    $alert = "DNI ingresado ya existe.";
                    require "Views/agregarTitular.view.php";
                }
            }else{
                $alert = "Ingrese todos los campos con (*)";
                require "Views/agregarTitular.view.php";
            }

        }else{
            require "Views/agregarTitular.view.php";
        }
    }

    public function traerTitulares(){
        Funciones::comprobarSesion();

        $titulares = $this->DAO_Persona->traerListado();
//        Funciones::mostrarTodo($titulares);
        require "Views/titulares.view.php";
    }

}
