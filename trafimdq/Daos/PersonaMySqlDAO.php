<?php namespace Daos;

//use Daos\conexion;
//use Daos\IDAO;
use Config\Funciones;
use Models\persona;
use PDO;


class PersonaMySqlDAO implements IDAO{

    protected $listado;
    protected $persona;
    protected $id;
    private static $instance = null;

    public static function getInstance() {
        if (self::$instance === NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct(){

    }

    public function agregar($user){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();
        $id = uniqid();

        $sql ="call PA_insertarUsuario('$id',
                                '{$user->get('password')}',
                                '{$user->get('email')}',
                                '{$user->get('perfil')}',
                                '{$user->get('estado')}');
                                set @fn = STR_TO_DATE('{$user->get('fecha_nacimiento')}', '%d-%m-%Y');
                                set @fa = NOW();
                                call PA_insertarPersona('$id',
                                '{$user->get('dni')}',
                                '{$user->get('nombre')}',
                                '{$user->get('domicilio')}',
                                '{$user->get('telefono')}',
                                '{$user->get('foto')}',
                                @fn,
                                '{$user->get('apellido')}',
                                @fa);";

        //        Funciones::mostrarTodo($sql);
        $stm = $conexion->prepare($sql);
        $stm->execute();

    }
    public function modificar($persona){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql =
            "set @fn = STR_TO_DATE('{$persona->get('fecha_nacimiento')}', '%d-%m-%Y');
            call PA_modificarPersona('{$persona->get('nombre')}',
            '{$persona->get('apellido')}',
            '{$persona->get('dni')}',
            '{$persona->get('domicilio')}',
            @fn,
            '{$persona->get('foto')}',
            '{$persona->get('telefono')}',
            '{$persona->get('perfil')}',
            '{$persona->get('estado')}',
            '{$persona->get('email')}',
            '{$persona->get('password')}');";

        //        Funciones::mostrarTodo($sql);
        $stm = $conexion->prepare($sql);
        $stm->execute();
    }
    public function traerListado($v=""){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerPersonas();";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($stm)){
            return ($stm) ? $stm : false;
        }
    }

    public function traerPersonaPorDni($dni){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerPersonaPorDni($dni);";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetch(PDO::FETCH_ASSOC);

        if(!empty($stm)){
            $stm = $this->mapear($stm);
            return ($stm) ? $stm : false;
        }
    }

    public function traerPorEmail($email){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerUsuarioPorEmail('$email');";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetchAll();
        //        Funciones::mostrarTodo($sql);

        if(!empty($stm)){
            $stm = $stm[0];
            $persona = new Persona();
            $persona->set('estado',$stm["estado"]);
            $persona->set('email',$stm['email']);
            $persona->set('password',$stm['pass']);
            $persona->set('dni',$stm['dni']);
            $persona->set('perfil',$stm['perfil']);

            //            Funciones::mostrarTodo($stm);
            return ($persona) ? $persona : false;
        }
    }
    public function mapear($p) {

        $Persona  = new Persona();
        $Persona->set('nombre',$p['nombre']);
        $Persona->set('apellido',$p['apellido']);
        $Persona->set('estado',$p['estado']);
        $Persona->set('dni',$p['dni']);
        $Persona->set('domicilio',$p['direccion']);
        $Persona->set('fecha_alta',$p['fecha_alta']);
        $Persona->set('fecha_nacimiento',$p['fecha_nacimiento']);
        $Persona->set('email',$p['email']);
        $Persona->set('password',$p['pass']);
        $Persona->set('perfil',$p['perfil']);
        $Persona->set('foto',$p['foto']);
        $Persona->set('telefono',$p['telefono']);
        //        Funciones::mostrarTodo($Persona);
        return $Persona;

    }
}
