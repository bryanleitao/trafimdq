<?php namespace Config;

class Autoload{
    public static function run(){
        spl_autoload_register(function($class){
            $class = ROOT . $class;
            $ruta = str_replace("\\","/", $class). ".php";
            require $ruta;
        });
    }
}
