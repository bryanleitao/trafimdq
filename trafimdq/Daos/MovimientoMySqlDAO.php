<?php namespace Daos;

use Daos\IMovimientoDAO;
use Daos\conexion;
use PDO;
use Models\vehiculo;
use Models\cuenta;
use Models\movimiento;
use Config\Funciones;
use Config\Config;
/**
 * Esta clase deberia ser SINGLETON sino no funcionaria correctamente
 * de la manera en que está programada. No crear más de una instancia.
 *
 * ¿Por qué?
 */
class MovimientoMySqlDAO implements IMovimientoDAO{
    protected $ruta;
    protected $listado;
    private static $instance = null;

    public static function getInstance() {
        if (self::$instance === NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**
     * Constructor
     *
     * @param string $ruta Ruta del archivo "base de datos"
     */
    public function __construct(){

    }

    public function modificar($v){

    }

    public function traerListado($dni){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = " call PA_traerMovimientos('$dni');";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetchAll(PDO::FETCH_ASSOC);
        //        Funciones::mostrarTodo($stm);
        if(!empty($stm)){
            return ($stm) ? $stm : false;
        }
    }

    public function comprobarDominio($dni,$dominio){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = " call PA_comprobarDominio('$dni','$dominio');";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetch(PDO::FETCH_ASSOC);

        if(!empty($stm)){
            return ($stm) ? $stm['existe'] : false;
        }
    }

    public function traerMovimientoPorDominio($dominio){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerMovimientosPorDominio('$dominio');";
        //        $sql = "call PA_traerMovimientosPorDominio('$dominio','$inicio',{MOV_POR_PAGINA});";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($stm)){
            //            $v = new Vehiculo();
            //            $c = new Cuenta();
            //            $v->set('dominio',$dominio);
            //            foreach($stm as $s){
            //                $movimiento  = new Movimiento();
            //                foreach($s as $key => $value){
            //                    $movimiento->set($key,$value);
            //                }
            //                $m[] = $movimiento;
            //            }
            //            $c->set('movimientos',$m);
            //            $v->set('cuenta',$c);
            //            return ($v) ? $v : false;
            return ($stm) ? $stm : false;
        }
    }

    public function simular($v,$m){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_insertarMovimiento('{$v->get('dominio')}','{$m->get('movimiento')}','{$m->get('importe')}');";
        $stm = $conexion->prepare($sql);
//        Funciones::mostrarTodo($stm);
        $stm->execute();
    }
}
