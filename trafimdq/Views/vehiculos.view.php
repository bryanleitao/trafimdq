<div class="container mar-v-md">
    <!--                Automotor                -->
    <h3 class="cl-dark"><a href="<?php echo ROOTFOLDERS ?>TitularController/">Titular</a> <i class="fa fa-angle-right"></i> Vehiculos</h3>

    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <div class="panel panel-info">
                <form method="POST" id="formVehiculo" action="<?= ROOTFOLDERS ?>VehiculoController/accion">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-heading">
                                <?php if(isset($persona)):?>
                                <h4 class="pull-right">Fecha de alta:
                                    <?php echo $persona->get('fecha_alta');?>
                                </h4>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="row mar-v-sm mar-h-sm">
                        <div class="col-md-4">
                            <div class="row ">
                                <div class="col-md-12">
                                    <output id="list">
                                        <?php if(isset($persona)):?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/" . $persona->get('foto');?>">
                                        <?php else: ?>
                                        <img class="img-avtr img-responsive center-block" src="<?php echo URL_IMG . "titulares/imgdefault.png"?>">
                                        <?php endif; ?>
                                    </output>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get('nombre');?> <?php if(isset($persona)) echo $persona->get("apellido");?></label>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get("domicilio");?></label>
                                </div>
                                <div class="col-md-12">
                                    <label for="#"><?php if(isset($persona)) echo $persona->get("telefono");?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Editar</th>
                                        <th>Dominio</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Año</th>
                                        <th>Saldo</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if(!empty($persona->get('vehiculos'))):foreach($persona->get('vehiculos') as $v):?>
                                    <tr>
                                        <td><input type="radio" name="editar" id="rad1" value="<?= $v->get('dominio')?>" checked></td>
                                        <td><?= $v->get('dominio')?></td>
                                        <td><?= $v->get('marca')?></td>
                                        <td><?= $v->get('modelo')?></td>
                                        <td><?= $v->get('anio')?></td>
                                        <td>$<?= $v->get('cuenta')->get('saldo')?></td>
                                    </tr>
                                    <?php endforeach; endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row mar-h-sm">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <button type='submit' value='modificar' name='modificar' form='formVehiculo' class='btn btn-warning'<?php if(empty($persona->get('vehiculos'))) echo "disabled";?>>
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type='submit' value='movimientos' name='movimientos' form='formVehiculo' class='btn btn-warning'<?php if(empty($persona->get('vehiculos'))) echo "disabled";?>>
                                    <i class="fa fa-list"></i>
                                </button>
                                <button type='submit' value='eliminar' name='eliminar' form='formVehiculo' class='btn btn-warning'<?php if(empty($persona->get('vehiculos'))) echo "disabled";?>>
                                    <i class="fa fa-trash"></i>
                                </button>
                                <a class="btn btn-warning" href="<?php echo ROOTFOLDERS ?>VehiculoController/agregar">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->

