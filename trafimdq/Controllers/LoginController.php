<?php namespace Controllers;

use Models\persona;
use Daos\UsuarioMysqlDAO;

class LoginController{

    private $users = null;

    public function __construct(){
        $this->users = UsuarioMysqlDAO::getInstance();
        $alert = "";
    }

    /*
   *
   * valida las sesiones, y evalua q tipo de perfil tiene, dependiendo el perfil q tiene son los permisos que se van a mostrar en la pagina
   *
    */
    public function index(){
//        $alert = $this->getAlert();
        if(!isset($_SESSION['usuario'])){
            require "Views/login.view.php";
        }else{
            $u = $_SESSION['usuario'];
            // mando al index del usuario(depende del perfil)

            if($u->get("perfil") == "Administrador"){
                $location = "Location: " . ROOTFOLDERS . "AdminController/index";
                header($location);
            }

            if($u->get("perfil") == "Empleado"){
                $location = "Location: " . ROOTFOLDERS . "AdminController/index";
                header($location);
            }

            if($u->get("perfil") == "Titular"){
            }
        }
    }
    /*
   *
   * valida los datos traidos por parametro y trae el usuario q se solicita
   * @param $username string puede ser un dni o un email
   * @param $password string
   *
    */
    public function loguear($username = "",$password = ""){
        if(!isset($_REQUEST['GET']) or !isset($_SESSION)){
            if(!empty($username) and !empty($password)){

                if(filter_var($username, FILTER_VALIDATE_EMAIL)){
                    $user = $this->users->traerUsuarioPorEmail($username);
                }else{
                    $user = $this->users->traerUsuarioPorDni($username);
                }

                if($user != null){
                    if($user->get("password") == $password){
                        $_SESSION['usuario'] = $user;
                        $location = "Location: " . ROOTFOLDERS;
                        header($location);
                    }else{
                        $this->alert .= "Password invalido.";
                    }
                }else{
                    $this->alert .= "E-mail o Usuario invalido.";
                }
            }else{
                $this->alert .= "Complete el Usuario y Password.";
            }

            $alert = $this->getAlert();
            require "Views/login.view.php";
        }else{
            $location = "Location: " . ROOTFOLDERS;
            header($location);
        }
    }
    public function getAlert(){
        return $this->alert;
    }
    /*
   *
   * destruye la sesion y se desloguea
   *
    */
    public function salir() {
        session_destroy();
        header("Location: " . ROOTFOLDERS );
    }
}
