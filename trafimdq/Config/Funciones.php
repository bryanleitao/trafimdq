<?php namespace Config;

class Funciones{

    static function mostrarTodo($valor){
        echo "<pre>";
        print_r($valor);
        echo "</pre>";
    }

    static function comprobarSesion(){
//        Funciones::mostrarTodo($_SESSION['usuario']->get('perfil'));
        if(!isset($_SESSION) or $_SESSION['usuario'] == null or $_SESSION['usuario']->get('perfil') == "Titular"){
            $location = "Location: " . ROOTFOLDERS;
            header($location);
        }
    }

    static function redireccionar(){
        $_SESSION['dni'] = null;
        $location = "Location: " . ROOTFOLDERS;
        header($location);

    }
}
