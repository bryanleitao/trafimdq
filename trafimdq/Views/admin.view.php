<div class="container mar-v-lg">
    <div class="row">
        <form method="POST" action="<?= ROOTFOLDERSADMIN ?>buscar/">
            <div class="col-md-offset-4 col-md-4">
                <input type="text" class="form-control" name="dni" placeholder="Buscar por DNI">
            </div>
            <div class="col-md-offset-4 col-md-4 text-right mar-v-md">
                <input type="submit" name="search" class="btn btn-info" value="Buscar">
            </div>
            <?php if(!empty($alert)):?>
            <label class="alert-danger" for="error"><?= $alert; ?></label>
            <?php endif;?>
        </form>
    </div>
</div>
<!-- /.container -->

