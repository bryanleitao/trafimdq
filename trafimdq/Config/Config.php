<?php namespace Config;
use PDO;
//define('ROOT', dirname(dirname(__FILE__)) . DS); //str_replace("/", "\\", $_SERVER['CONTEXT_DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI']));
//define('__ROOT__', dirname(dirname(__FILE__)));


// Constantes Base de datos
define("DB_SERVER", "localhost");
define("DB_NAME", "trafimdq");
define("DB_USER", "root");
define("DB_PASS", "");
define('DB_CHARSET','utf-8');
// Constantes front
define("PORT_1","8082"); //bryan
define('THEME_NAME', 'tema1');
define('DS',DIRECTORY_SEPARATOR);
//define('ROOT',realpath(dirname(__FILE__)) . DS);
define("ROOTFOLDERS","http://localhost:" . PORT_1 . "/trafimdq/trafimdq/");
define('ROOTFOLDERSADMIN', ROOTFOLDERS . "AdminController/");
define('ROOT', dirname(__DIR__) . DS);
define("URL_THEME", ROOTFOLDERS . "Views/");
define("URL_CSS", URL_THEME . "css/");
define("URL_JS", URL_THEME . "js/");
define("URL_IMG", URL_THEME . "img/");
define ("IMGDEFAULT","imgdefault.png");
// Constantes Server
define('HOST_ROOT', __DIR__ . '/../');
define('HOST_URL_THEME', HOST_ROOT . 'vistas/'. THEME_NAME . '/');
define('MOV_POR_PAGINA' , 8);


//echo '<p>Constante DS:' . DS . '</p>';
//echo '<p>Constante ROOT:' . ROOT . '</p>';
//echo '<p>Constante DB_NAME:' . DB_NAME . '</p>';
//echo '<p>Constante DB_USER:' . DB_USER . '</p>';
//echo '<p>Constante DB_PASS:' . DB_PASS . '</p>';
//echo '<p>Constante DB_HOST:' . DB_HOST . '</p>';
//echo '<p>Constante URL_THEME:' . URL_THEME . '</p>';
//echo '<p>Constante URL_CSS: ' . URL_CSS . '</p>';
//echo '<p>Constante URL_JS:' . URL_JS . '</p>';
//echo '<p>Constante THEME_NAME:' . THEME_NAME . '</p>';

//echo '<pre>';
//print_r($_SERVER);
//echo '</pre>';
