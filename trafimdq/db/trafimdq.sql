-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-12-2016 a las 03:28:38
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `trafimdq`
--
create database trafimdq;
use trafimdq;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `ID_CUENTA` int(11) NOT NULL,
  `FK_DOMINIO` varchar(7) NOT NULL,
  `SALDO` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`ID_CUENTA`, `FK_DOMINIO`, `SALDO`) VALUES
(1, 'NPO843', 0),
(2, 'ABC123', 6120),
(9, 'DFG456', 15),
(10, 'QWE123', 200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id` int(11) NOT NULL,
  `cuentas_ID_CUENTA` int(11) NOT NULL,
  `MOVIMIENTO` varchar(15) NOT NULL,
  `SALDO` double NOT NULL,
  `FECHA` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id`, `cuentas_ID_CUENTA`, `MOVIMIENTO`, `SALDO`, `FECHA`) VALUES
(1, 1, 'peaje', 20, '2016-11-10 15:39:59'),
(2, 1, 'peaje', 15, '2016-11-10 15:39:59'),
(3, 1, 'multa', 3000, '2016-11-10 15:39:59'),
(4, 2, 'multa', 600, '2016-11-10 15:39:59'),
(5, 1, 'multa', 1500, '2016-11-10 15:39:59'),
(6, 2, 'peaje', 25, '2016-11-10 15:39:59'),
(7, 1, 'multa', 1000, '2016-11-10 15:39:59'),
(8, 2, 'peaje', 20, '2016-11-10 15:39:59'),
(9, 2, 'peaje', 15, '2016-11-10 15:39:59'),
(10, 1, 'peaje', 20, '2016-11-10 15:39:59'),
(11, 1, 'peaje', 20, '2016-11-10 15:40:20'),
(12, 1, 'peaje', 15, '2016-11-10 15:40:20'),
(13, 1, 'multa', 3000, '2016-11-10 15:40:20'),
(14, 2, 'multa', 600, '2016-11-10 15:40:20'),
(15, 1, 'multa', 1500, '2016-11-10 15:40:20'),
(16, 2, 'peaje', 25, '2016-11-10 15:40:20'),
(17, 1, 'multa', 1000, '2016-11-10 15:40:20'),
(18, 2, 'peaje', 20, '2016-11-10 15:40:20'),
(19, 2, 'peaje', 15, '2016-11-10 15:40:20'),
(20, 1, 'peaje', 20, '2016-11-10 15:40:20'),
(21, 1, 'pago', -70, '2016-11-21 17:58:14'),
(23, 1, 'pago', -500, '2016-11-22 03:15:29'),
(24, 1, 'pago', -500, '2016-11-22 03:28:47'),
(25, 1, 'pago', -500, '2016-11-22 03:29:31'),
(26, 1, 'pago', -500, '2016-11-22 03:30:41'),
(27, 1, 'pago', -500, '2016-11-22 03:38:06'),
(28, 1, 'pago', -50, '2016-11-22 19:12:14'),
(29, 1, 'peaje', 50, '2016-11-22 19:12:29'),
(30, 1, 'peaje', 15, '2016-11-22 19:14:16'),
(31, 1, 'multa', 150, '2016-11-22 19:14:57'),
(36, 2, 'pago', -200, '2016-11-22 20:51:24'),
(37, 1, 'multa', 300, '2016-11-22 20:52:30'),
(38, 1, 'pago', -5, '2016-11-22 20:52:56'),
(39, 2, 'multa', 5000, '2016-11-22 21:06:20'),
(40, 9, 'multa', 125, '2016-11-27 21:19:50'),
(41, 9, 'pago', -150, '2016-11-27 21:20:13'),
(42, 1, 'pago', -6120, '2016-11-28 01:12:52'),
(43, 1, 'pago', -2880, '2016-11-28 01:13:33'),
(44, 10, 'peaje', 100, '2016-12-04 03:09:34'),
(45, 10, 'multa', 600, '2016-12-04 23:30:41'),
(46, 10, 'pago', -500, '2016-12-04 23:31:10'),
(47, 9, 'peaje', 40, '2016-12-15 00:20:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `FK_usuario` varchar(50) NOT NULL,
  `DNI_TITULAR` int(11) NOT NULL,
  `FECHA_ALTA` datetime NOT NULL,
  `NOMBRE` varchar(50) NOT NULL,
  `DOMICILIO` varchar(50) NOT NULL,
  `TELEFONO` varchar(50) NOT NULL,
  `FOTO` varchar(30) NOT NULL,
  `FECHA_NACIMIENTO` date NOT NULL,
  `APELLIDO` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`FK_usuario`, `DNI_TITULAR`, `FECHA_ALTA`, `NOMBRE`, `DOMICILIO`, `TELEFONO`, `FOTO`, `FECHA_NACIMIENTO`, `APELLIDO`) VALUES
('1', 12345, '2016-11-08 10:21:27', 'NombAdmin', 'Admin', '1234567', '5834c79818506.jpg', '1925-11-08', 'ApeAdmin'),
('583c56bfa79ec', 98765, '2016-11-28 13:09:35', 'Oscar', 'UTN', '3456789', '583c56bfa6ce6.jpg', '1991-01-01', 'Botteri'),
('5834ddf3692ea', 123456, '2016-11-22 21:08:19', 'adrian', 'UTN', '123456789', '583c567e7bcbe.jpg', '1980-03-01', 'solimano'),
('58221e131fa8f', 35621041, '2016-11-08 15:48:51', 'Bryan ', 'Mexico 762', '223-5487890', '58311e6a6acdd.jpg', '1991-02-22', 'Leitao');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Empleado'),
(3, 'Titular');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `FK_id_rol` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `password`, `email`, `FK_id_rol`, `estado`) VALUES
('1', '12345', 'admin@admin.com', 1, 1),
('58221e131fa8f', '35621041', 'bryanleitao1991@gmail.com', 2, 1),
('5833e8878d705', '36616955', 'sofia@mail.com', 1, 2),
('5834ddf3692ea', '123456', 'asdasd@asdasd.com', 3, 1),
('583c56bfa79ec', '98765', 'mail2@mail.com', 3, 1),
('583cd590b8289', '35621041', '', 1, 2),
('583cd617d6bc4', '35621041', 'sofia@mail.com', 1, 2),
('583cd69950b00', '35621041', '', 1, 2),
('583cd6d0e001a', '35621041', '', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `DOMINIO` varchar(7) NOT NULL,
  `MARCA` varchar(50) NOT NULL,
  `MODELO` varchar(50) NOT NULL,
  `ANIO` int(11) NOT NULL,
  `FK_QR` varchar(50) NOT NULL,
  `FK_DNI_TITULAR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`DOMINIO`, `MARCA`, `MODELO`, `ANIO`, `FK_QR`, `FK_DNI_TITULAR`) VALUES
('ABC123', 'Lucky Lion', 'Bumblebee', 2010, '35621041abc123', 35621041),
('DFG456', 'Ferrari', 'Rojo', 2005, '123456asd123', 123456),
('NPO843', 'Chevrolet', 'Corsa 5 puertas', 2015, '35621041npo843', 35621041),
('QWE123', 'FIAT', 'UNO', 2008, '98765QWE123', 98765);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`ID_CUENTA`),
  ADD KEY `FK_DOMINIO` (`FK_DOMINIO`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_movimientos_cuentas_idx` (`cuentas_ID_CUENTA`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`DNI_TITULAR`),
  ADD UNIQUE KEY `FK_usuario` (`FK_usuario`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`),
  ADD KEY `FK_rol` (`FK_id_rol`) USING BTREE;

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`DOMINIO`),
  ADD KEY `FK_DNI_TITULAR` (`FK_DNI_TITULAR`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `ID_CUENTA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD CONSTRAINT `FK_CUENTA_VEHICULO` FOREIGN KEY (`FK_DOMINIO`) REFERENCES `vehiculos` (`DOMINIO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD CONSTRAINT `fk_movimientos_cuentas` FOREIGN KEY (`cuentas_ID_CUENTA`) REFERENCES `cuentas` (`ID_CUENTA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
  ADD CONSTRAINT `personas_ibfk_1` FOREIGN KEY (`FK_usuario`) REFERENCES `usuarios` (`usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`FK_id_rol`) REFERENCES `roles` (`id_rol`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `vehiculos_ibfk_1` FOREIGN KEY (`FK_DNI_TITULAR`) REFERENCES `personas` (`DNI_TITULAR`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_comprobarDominio` (`dni` INT, `dominio` VARCHAR(7))  BEGIN
set @hay = (select COUNT(m.id) from movimientos m
INNER join cuentas c on m.cuentas_ID_CUENTA = c.ID_CUENTA
INNER join vehiculos v on c.FK_DOMINIO = v.DOMINIO
INNER join personas p on p.DNI_TITULAR = v.FK_DNI_TITULAR
WHERE p.DNI_TITULAR = dni and v.DOMINIO = dominio );

if @hay > 0 THEN set @existe = true;
ELSE set @existe = false;
end if;

select @existe as existe;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_crearVehiculo` (IN `dominio` VARCHAR(7), IN `marca` VARCHAR(50), IN `modelo` VARCHAR(50), IN `anio` INT, IN `dni` INT, IN `qr` VARCHAR(50))  BEGIN
	call PA_insertarVehiculo(dominio,marca,modelo,anio,dni,qr);
    call PA_insertarCuenta(dominio);
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_insertarCuenta` (IN `dominio` VARCHAR(7))  NO SQL
INSERT INTO cuentas(FK_DOMINIO,SALDO) VALUES(dominio,0)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_insertarMovimiento` (IN `dominio` VARCHAR(7), IN `movimiento` VARCHAR(50), IN `importe` DOUBLE)  BEGIN
set @cuenta = (select c.ID_CUENTA FROM cuentas c where c.FK_DOMINIO = dominio);
if movimiento = 'pago' then set importe = importe * -1;
end if;
insert into movimientos(cuentas_ID_CUENTA, MOVIMIENTO, SALDO, FECHA) 
values(@cuenta,movimiento,importe,now());
UPDATE cuentas SET SALDO = ((select SALDO WHERE ID_CUENTA = @cuenta) + importe)
WHERE ID_CUENTA = @cuenta;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_insertarPersona` (IN `id` VARCHAR(25), IN `dni` INT, IN `nombre` VARCHAR(50), IN `domicilio` VARCHAR(50), IN `telefono` VARCHAR(50), IN `foto` VARCHAR(30), IN `fecha_nacimiento` DATE, IN `apellido` VARCHAR(30), IN `fecha_alta` DATETIME)  NO SQL
INSERT INTO personas 
VALUES(id,dni,fecha_alta,nombre,domicilio,telefono,foto,fecha_nacimiento,apellido)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_insertarUsuario` (IN `id` VARCHAR(25), IN `pass` VARCHAR(25), IN `email` VARCHAR(50), IN `idRol` INT, IN `estado` INT)  NO SQL
INSERT INTO usuarios 
VALUES(id,
       pass,
       email,
       idRol,
       estado)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_insertarVehiculo` (INOUT `dominio` VARCHAR(7), IN `marca` VARCHAR(50), IN `modelo` VARCHAR(50), IN `anio` INT, IN `dni` INT, IN `qr` VARCHAR(50))  NO SQL
INSERT INTO `vehiculos`(`DOMINIO`, `MARCA`, `MODELO`, `ANIO`,`FK_QR`, `FK_DNI_TITULAR`) VALUES (dominio,marca,modelo,anio,qr,dni)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_modificarPersona` (IN `nombre` VARCHAR(50), IN `apellido` VARCHAR(30), IN `dni` INT, IN `domicilio` VARCHAR(50), IN `fecha_nacimiento` DATE, IN `foto` VARCHAR(30), IN `telefono` VARCHAR(50), IN `perfil` INT, IN `estado` INT, IN `email` VARCHAR(50), IN `pass` VARCHAR(25))  NO SQL
UPDATE personas AS p
INNER JOIN usuarios AS u ON p.FK_usuario = u.usuario
SET p.NOMBRE = nombre,
p.APELLIDO = apellido,
p.DOMICILIO = domicilio,
p.FECHA_NACIMIENTO = fecha_nacimiento,
p.FOTO = foto,
p.TELEFONO = telefono,
u.FK_id_rol = perfil,
u.estado = estado,
u.email = email,
u.password = pass
WHERE p.DNI_TITULAR = dni$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_modificarSaldo` (IN `dominio` VARCHAR(7), IN `saldo` FLOAT)  BEGIN
SET SQL_SAFE_UPDATES = 0;
UPDATE cuentas SET SALDO = saldo
WHERE FK_DOMINIO = dominio;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_modificarVehiculo` (IN `marca` VARCHAR(50), IN `modelo` VARCHAR(50), IN `anio` INT, IN `dominio` VARCHAR(7))  NO SQL
UPDATE `vehiculos` SET `MARCA` = marca, `MODELO` = modelo, `ANIO` = anio WHERE `vehiculos`.`DOMINIO` = dominio$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerMovimientos` (IN `dni` INT)  NO SQL
SELECT v.DOMINIO dominio, m.MOVIMIENTO movimiento, m.SALDO importe, DATE_FORMAT(m.FECHA,'%d/%m/%Y %H:%m:%s') fecha 
FROM movimientos m 
INNER JOIN cuentas c on c.ID_CUENTA = m.cuentas_ID_CUENTA INNER JOIN vehiculos v on v.DOMINIO = c.FK_DOMINIO 
WHERE v.FK_DNI_TITULAR = dni order by fecha desc LIMIT 15$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerMovimientosPorDominio` (IN `dominio` VARCHAR(7))  NO SQL
SELECT v.DOMINIO dominio, m.MOVIMIENTO movimiento, m.SALDO importe, DATE_FORMAT(m.FECHA,'%d/%m/%Y %H:%m:%s') fecha 
FROM movimientos m 
INNER JOIN cuentas c on c.ID_CUENTA = m.cuentas_ID_CUENTA 
INNER JOIN vehiculos v on v.DOMINIO = c.FK_DOMINIO 
WHERE v.DOMINIO = dominio
order by fecha desc
limit 10$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerPersonaPorDni` (IN `dni` INT)  NO SQL
SELECT  u.estado AS estado,
u.usuario AS id,
p.NOMBRE as nombre,
p.apellido as apellido,
p.DNI_TITULAR as dni,
p.DOMICILIO as direccion,
p.FECHA_ALTA as fecha_alta,
DATE_FORMAT(p.FECHA_NACIMIENTO,'%d/%m/%Y') AS fecha_nacimiento,
u.email as email,
u.password as pass,
r.rol as perfil,
p.FOTO as foto,
p.TELEFONO as telefono
FROM personas as p
INNER JOIN usuarios as u ON u.usuario = p.FK_usuario
INNER JOIN roles as r ON u.FK_id_rol = r.id_rol
WHERE p.DNI_TITULAR = dni$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerPersonas` ()  NO SQL
SELECT p.FOTO foto, CONCAT(p.NOMBRE, ' ', p.APELLIDO) nomb_ape, p.DNI_TITULAR dni, p.DOMICILIO domicilio, p.TELEFONO telefono,
DATE_FORMAT(p.FECHA_NACIMIENTO,'%d/%m/%Y') AS fecha_nacimiento, u.email email, u.password pass, r.rol rol
FROM personas p 
INNER JOIN usuarios u ON u.usuario = p.FK_usuario
INNER JOIN roles r ON r.id_rol = u.FK_id_rol
ORDER BY nomb_ape$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerSaldo` (IN `dominio` VARCHAR(7))  BEGIN
call PA_traerSaldoCuenta(dominio,@saldo);
call PA_modificarSaldo(dominio,@saldo);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerSaldoCuenta` (IN `dominio` VARCHAR(7), OUT `saldo` FLOAT)  NO SQL
set saldo = (SELECT  sum(m.SALDO) saldo FROM movimientos m INNER JOIN cuentas c on c.ID_CUENTA = m.cuentas_ID_CUENTA INNER JOIN vehiculos v on v.DOMINIO = c.FK_DOMINIO WHERE v.DOMINIO = dominio)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerUsuarioPorDni` (IN `dni` INT)  NO SQL
SELECT  u.estado AS estado,
u.email as email,
u.password as pass,
p.DNI_TITULAR as dni,
r.rol as perfil
FROM usuarios as u
INNER JOIN personas as p ON u.usuario = p.FK_usuario
INNER JOIN roles as r ON u.FK_id_rol = r.id_rol
WHERE p.DNI_TITULAR = dni$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerUsuarioPorEmail` (IN `email` VARCHAR(50))  NO SQL
SELECT  u.estado AS estado,
u.email as email,
u.password as pass,
p.DNI_TITULAR as dni,
r.rol as perfil
FROM usuarios as u
INNER JOIN personas as p ON u.usuario = p.FK_usuario
INNER JOIN roles as r ON u.FK_id_rol = r.id_rol
WHERE u.email = email$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerVehiculoPorDominio` (IN `dom` VARCHAR(7))  NO SQL
select v.DOMINIO as dominio, v.MARCA as marca, v.MODELO as modelo, v.ANIO as anio 
from vehiculos as v 
WHERE v.DOMINIO = dom$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `PA_traerVehiculos` (IN `dni` VARCHAR(11))  SELECT v.DOMINIO dominio, v.MARCA marca, v.MODELO modelo, v.ANIO anio, c.SALDO saldo
FROM vehiculos as v, cuentas as c 
WHERE v.FK_DNI_TITULAR = dni and c.FK_DOMINIO = v.DOMINIO
ORDER by saldo DESC$$

DELIMITER ;
