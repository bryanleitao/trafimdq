<?php namespace Daos;

use Daos\IVehiculoDAO;
use Daos\conexion;
use Models\vehiculo;
use Models\cuenta;
use PDO;
use Config\Funciones;
/**
 * Esta clase deberia ser SINGLETON sino no funcionaria correctamente
 * de la manera en que está programada. No crear más de una instancia.
 *
 * ¿Por qué?
 */
class VehiculoMySqlDAO implements IVehiculoDAO{
    protected $listado;
    private static $instance = null;

    public static function getInstance() {
        if (self::$instance === NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    /**
     * Constructor
     *
     * @param string $ruta Ruta del archivo "base de datos"
     */
    public function __construct(){

    }

    public function agregar($dni,$v){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();
        $qr = $dni . $v->get('dominio');
        //                    Funciones::mostrarTodo($qr);

        $sql = "call PA_crearVehiculo('{$v->get('dominio')}','{$v->get('marca')}','{$v->get('modelo')}','{$v->get('anio')}',$dni,'$qr')";
        $stm = $conexion->prepare($sql);
        $stm->execute();
        //        Funciones::mostrarTodo($sql);
    }

    public function modificar($v){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql =
            "call PA_modificarVehiculo('{$v->get('marca')}',
            '{$v->get('modelo')}',
            '{$v->get('anio')}',
            '{$v->get('dominio')}');";

        //        Funciones::mostrarTodo($sql);
        $stm = $conexion->prepare($sql);
        $stm->execute();
    }
    public function eliminarVehiculo($dominio){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "DELETE FROM vehiculos WHERE DOMINIO = '$dominio'";

        //                Funciones::mostrarTodo($sql);
        $stm = $conexion->prepare($sql);
        $stm->execute();
    }

    public function traerListado($v=""){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "select dominio,FK_DNI_TITULAR AS dni from vehiculos";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetchAll(PDO::FETCH_ASSOC);
        //        Funciones::mostrarTodo($stm);

        if(!empty($stm)){

            foreach ($stm as $s){
                $vehiculo = new Vehiculo();
                foreach ($s as $key => $value){
                    $vehiculo->set($key,$value);
                }
                $v[]=$vehiculo;
            }
            return ($v) ? $v : false;
        }
    }


    public function traerVehiculos($dni){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerVehiculos('$dni')";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($stm)){
            $vehiculos = null;
            foreach ($stm as $s)
            {
                $vehiculo = new Vehiculo();

                foreach ($s as $key => $value)
                {
                    if($key == "saldo"){
                        $cuenta = new Cuenta();
                        $cuenta->set('saldo',$value);
                        $vehiculo->set('cuenta',$cuenta);
                    }else{
                        $vehiculo->set($key,$value);
                    }
                }

                $vehiculos[] = $vehiculo;
            }
            return ($vehiculos) ? $vehiculos : false;
        }
    }

    public function traerVehiculo($dominio){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerVehiculoPorDominio('$dominio')";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetch(PDO::FETCH_ASSOC);
        //        Funciones::mostrarTodo($stm);

        if(!empty($stm)){
            $vehiculo = new Vehiculo();
            foreach ($stm as $key => $value){
                $vehiculo->set($key,$value);
            }

            return ($vehiculo) ? $vehiculo : false;
        }
    }

}
