<?php namespace allDaos\daos;
use config\Constantes;
use config\Conexion;
use allDaos\idaos\IPdoDAO;
use allDaos\daos\CuentasDAO;
use modelos\Vehiculo;
use modelos\Cuenta;
use PDO;

class VehiculosDAO extends Conexion implements IPdoDAO 
{
	protected $cuentas;
	protected $tabla = "vehiculos";
	private static $instance = null;

	public static function getInstance() {
		if (self::$instance === NULL) {
			self::$instance = new self();
			}
			return self::$instance;
	}

	private function autoBind($params, $sentencia) {
		foreach ($params as $key => &$val) {
			$sentencia->bindParam(':'. $key, $val);
		}
		return $sentencia;
	}

	public function validarDominio($value) {
		$sql = "SELECT dominio FROM " . $this->tabla . " WHERE dominio = " .'\''. $value .'\'';
		$sentencia = $this->execPDO($sql);
		$exists    = $sentencia->rowCount();
		return $exists;
	} 

	public function agregar($value) {
		$sql = "INSERT INTO " . $this->tabla . "(dominio, marca, modelo, qr, id_titular_fk, id_cuenta_fk) VALUES (:dominio, :marca, :modelo, :qr, :id_titular_fk, :id_cuenta_fk)";
		
		$parametros = $value->toArray(array('cuenta','id'));
		$parametros['id_cuenta_fk'] = $value->getCuenta()->getId();
		$parametros['id_titular_fk'] = $_SESSION['id'];

		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia = $this->autoBind($parametros,$sentencia);
		$sentencia->execute();
		
		$id = $conexion->lastInsertId();
		$vehiculo = $this->traerUno($id);
		
		return $vehiculo;	
	}

	public function todos() {
		$sql = "SELECT * FROM " . $this->tabla;
		$sentencia = $this->execPDO($sql);
		$array = [];
		$listado = NULL;
		while ($row = $sentencia->fetch()) {
			$array[]   = $row;
			$listado   = $this->mapear($array);	
		}
		$listado = $this->setCuenta($listado,$array);
		return $listado;
	}

	private function setCuenta($listado, $array) {
		$this->cuentas = CuentasDAO::getInstance();

		foreach ($listado as $vehiculo) {			
			$var 	= array_shift($array);
			$cuenta = $this->cuentas->traerUno($var['id_cuenta_fk']);
			$vehiculo->setCuenta($cuenta);			
		}
		return $listado;
	}

	public function traerPorTit($value){
		$sql 	       = "SELECT * FROM " . $this->tabla ." WHERE id_titular_fk = " . $value;
		$sentencia = $this->execPDO($sql);
		$array = [];
		$listado = NULL;
		while ($row = $sentencia->fetch()) {
			$array[] = $row;
		}
		$listado = $this->mapear($array);		
		foreach ($listado as $vehiculo) {			
			$vehiculo = $this->setCuenta($listado,$array);
		}
		return $listado;
	}

	public function traerUno($value){
		$sql = "SELECT * FROM " . $this->tabla ." WHERE id_vehiculo = " . $value;
		$sentencia = $this->execPDO($sql);
		$arrayVehiculo[] = $sentencia->fetch();
		$vehiculo = $this->mapear($arrayVehiculo);
		$vehiculo = array_shift($vehiculo);
		return $vehiculo;
	}	

	public function mapear($value) { 
		$value = array_map(function($p){	
			return new Vehiculo($p['id_vehiculo'], $p['dominio'], $p['marca'], $p['modelo'], $p['qr']);
			}, $value);
		return $value;
	}
	
	public function actualizar($value) {
		//no fue implementada
	
	}

	public function eliminar($value) {
		//no se utiliza
		$sql = "DELETE FROM " . $this->tabla . " WHERE id_vehiculo = " . $value;
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
	}

	private function execPDO($sql) {
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		return $sentencia;
	}
}
	/*public function getUltimoId(){
		$sql = "SELECT MAX(id_vehiculo) as id FROM " . $this->tabla;
		$obj_pdo = new Conexion();
		$conexion = $obj_pdo->conectar();
		$sentencia = $conexion->prepare($sql);
		$sentencia->execute();
		$lastId = $sentencia->fetch();
		$id = $lastId['id'];
		return $id;
	}*/