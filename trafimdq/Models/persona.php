<?php namespace Models;

use JsonSerializable;

class Persona extends ClaseConvertibleAArray implements JsonSerializable{

    protected $nombre;//
    protected $apellido;//
    protected $domicilio;//
    protected $dni;//
    protected $fecha_nacimiento;//
    protected $telefono;//
    protected $foto;//

    private $estado;//
    protected $email;//
    protected $password;//
    protected $perfil; // si admin, empleado, user..

    private $vehiculos;

    public function __construct(){

    }

    public function JsonSerialize() {
        $this->toArray();
    }
    public function get($atributo){
        return $this->$atributo;
    }
    public function set($atributo,$valor){
        $this->$atributo = $valor;
    }
}
