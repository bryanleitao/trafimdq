<?php
	session_start();

	require_once('config/Constantes.php');
	require_once('config/Autoload.php');
	require_once('vistas/includes/header.php');
	
	config\Autoload::iniciar();
	config\Router::direccionar(new config\Request());
	
	require_once('vistas/includes/footer.php');
	
	function pre($value) {
		echo "<pre>";
		print_r($value);
		echo "</pre>";
		exit();
	}

