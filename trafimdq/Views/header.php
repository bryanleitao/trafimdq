<!DOCTYPE html>
<html lang="en" ng-app="trafimdq">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>TrafiMDQ</title>

  <!-- Bootstrap -->
  <!--        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="<?php echo URL_CSS ?>/bs/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo URL_CSS ?>/style.css">
</head>

<body class="bg-primary">
  <header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= ROOTFOLDERS; ?>">
            <div class="span">
              <div class="icon"></div>
            </div>
          </a>
          <a class="navbar-brand" href="<?= ROOTFOLDERS; ?>">TrafiMDQ</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <?php if(isset($_SESSION['usuario'])):?>
            <ul class="nav navbar-nav navbar-right">
              <?php if($_SESSION['usuario']->get('perfil') == 'Administrador'):?>
                <li>
                  <a class="" href="<?php echo ROOTFOLDERS ?>SimulacionController">
                    <span class="glyphicon glyphicon-road" aria-hidden="true"></span> Simulacion
                  </a>
                </li>
                <li>
                  <a class="" href="<?php echo ROOTFOLDERS ?>TitularController/traerTitulares">
                    <i class="fa fa-list"></i> Titulares
                  </a>
                </li>
              <?php endif;?>
              <?php if($_SESSION['usuario']->get('perfil') != 'Titular'):?>
                <li>
                  <a class="" href="<?php echo ROOTFOLDERS ?>TitularController/agregar">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Titulares
                  </a>
                </li>
                <li>
                  <a class="" href="<?php echo ROOTFOLDERS ?>">
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar Titular
                  </a>
                </li>

              <?php endif;?>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cogs"></i> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo ROOTFOLDERS . "TitularController/editar/" . $_SESSION['usuario']->get('dni')?>">
                      <i class="fa fa-user"></i> Perfil
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo ROOTFOLDERS ?>LoginController/salir">
                      <span class="glyphicon glyphicon-off" aria-hidden="true"></span> Salir
                    </a>
                  </li>
                </ul> 
              </li>
            </ul>
          <?php else:?>
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a class="" href="#">
                  <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Recordar Password
                </a>
              </li>
            </ul>
          <?php endif; ?>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
  </header>
  <div class="content">

    <p>
      tengo{{20+6}} anios;
    </p>
