<?php namespace Models;

use JsonSerializable;

class Usuario extends ClaseConvertibleAArray implements JsonSerializable{

    private $estado;//
    protected $email;//
    protected $password;//
    protected $perfil; // si admin, empleado, user..

    public function __construct(){

    }

    public function JsonSerialize() {
        $this->toArray();
    }
    public function get($atributo){
        return $this->$atributo;
    }
    public function set($atributo,$valor){
        $this->$atributo = $valor;
    }
}
