<?php namespace Daos;

//use Daos\conexion;
//use Daos\IDAO;
use Config\Funciones;
use Models\persona;
use PDO;


class UsuarioMySqlDAO implements IUsuarioDAO{

    protected $listado;
    protected $persona;
    protected $id;
    private static $instance = null;

    public static function getInstance() {
        if (self::$instance === NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct(){

    }

    public function agregar($user){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();
        $id = uniqid();

        $sql ="call PA_insertarUsuario('$id',
                                '{$user->get('password')}',
                                '{$user->get('email')}',
                                '{$user->get('perfil')}');
                                set @fn = STR_TO_DATE('{$user->get('fecha_nacimiento')}', '%d-%m-%Y');
                                call PA_insertarPersona('$id',
                                '{$user->get('dni')}',
                                '{$user->get('nombre')}',
                                '{$user->get('domicilio')}',
                                '{$user->get('telefono')}',
                                '{$user->get('foto')}',
                                @fn,
                                '{$user->get('apellido')}',
                                NOW());";

        $stm = $conexion->prepare($sql);

        //        Funciones::mostrarTodo($p);

    }
    public function modificar($persona){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql =
            "set @fn = STR_TO_DATE('{$persona->get('fecha_nacimiento')}', '%d-%m-%Y');
            call PA_modificarPersona('{$persona->get('nombre')}',
            '{$persona->get('apellido')}',
            '{$persona->get('dni')}',
            '{$persona->get('domicilio')}',
            @fn,
            '{$persona->get('foto')}',
            '{$persona->get('telefono')}',
            '{$persona->get('perfil')}',
            '{$persona->get('estado')}',
            '{$persona->get('email')}',
            '{$persona->get('password')}',
            '$id');";
        //            "UPDATE personas AS p
        //                INNER JOIN usuarios AS u ON p.FK_usuario = u.usuario
        //                SET p.NOMBRE = '{$persona->get('nombre')}',
        //                p.APELLIDO = '{$persona->get('apellido')}',
        //                p.DNI_TITULAR = '{$persona->get('dni')}',
        //                p.DOMICILIO = '{$persona->get('domicilio')}',
        //                p.FECHA_NACIMIENTO = STR_TO_DATE('{$persona->get('fecha_nacimiento')}','%d/%m/%Y'),
        //                p.FOTO = '{$persona->get('foto')}',
        //                p.TELEFONO = '{$persona->get('telefono')}',
        //                u.FK_id_rol = '{$persona->get('perfil')}',
        //                u.estado = '{$persona->get('estado')}',
        //                u.email = '{$persona->get('email')}',
        //                u.password = '{$persona->get('password')}'
        //                WHERE p.FK_usuario = '$id'";


        //        Funciones::mostrarTodo($sql);
        $stm = $conexion->prepare($sql);
        $stm->execute();
    }
    public function traerListado($v){

    }

    public function traerUsuarioPorDni($dni){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerUsuarioPorDni($dni);";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetch(PDO::FETCH_ASSOC);
        //        Funciones::mostrarTodo($sql);

        if(!empty($stm)){
            $usuario = new Persona();
            $usuario->set('estado',$stm["estado"]);
            $usuario->set('email',$stm['email']);
            $usuario->set('password',$stm['pass']);
            $usuario->set('dni',$stm['dni']);
            $usuario->set('perfil',$stm['perfil']);

            //            Funciones::mostrarTodo($stm);
            return ($usuario) ? $usuario : false;
        }
    }

    public function traerUsuarioPorEmail($email){
        $conexion = new Conexion();
        $conexion = $conexion->conectar();

        $sql = "call PA_traerUsuarioPorEmail('$email');";
        $stm = $conexion->prepare($sql);
        $stm->execute();

        $stm = $stm->fetch(PDO::FETCH_ASSOC);
        //Funciones::mostrarTodo($sql);

        if(!empty($stm)){
            $usuario = new Persona();
            $usuario->set('estado',$stm["estado"]);
            $usuario->set('email',$stm['email']);
            $usuario->set('password',$stm['pass']);
            $usuario->set('dni',$stm['dni']);
            $usuario->set('perfil',$stm['perfil']);

            //            Funciones::mostrarTodo($stm);
            return ($usuario) ? $usuario : false;
        }
    }

}
